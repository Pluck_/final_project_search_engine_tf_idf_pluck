def cleaned_news():
    import pandas as pd
    import nltk
    import re
    from collections import defaultdict
    nltk.download('punkt')
    nltk.download('stopwords')
    from nltk.corpus import stopwords
    from nltk.stem import PorterStemmer
    ps =PorterStemmer()

    news = pd.read_json('https://raw.githubusercontent.com/zayedrais/DocumentSearchEngine/master/data/newsgroups.json')


    for i,txt in enumerate(news['content']):
        subject = re.findall('Subject:(.*\n)',txt)
    if (len(subject) !=0):
        news.loc[i,'Subject'] =str(i)+' '+subject[0]
    else:
        news.loc[i,'Subject'] ='NA'

    df_news =news[['Subject','content']]
    df_news.content =df_news.content.replace(to_replace='from:(.*\n)',value='',regex=True) 
    df_news.content =df_news.content.replace(to_replace='lines:(.*\n)',value='',regex=True)
    df_news.content =df_news.content.replace(to_replace='[!"#$%&\'()*+,/:;<=>?@[\\]^_`{|}~]',value=' ',regex=True) 
    df_news.content =df_news.content.replace(to_replace='-',value=' ',regex=True)
    df_news.content =df_news.content.replace(to_replace='\s+',value=' ',regex=True)
    df_news.content =df_news.content.replace(to_replace='  ',value='',regex=True)
    df_news.content =df_news.content.apply(lambda x:x.strip())
    df_news['content']=[i.lower() for i in df_news['content']]

    df_news['Word tokenize'] = [nltk.word_tokenize(i) for i in df_news.content]

    for i in df_news['Word tokenize']:
        for j in i:
            if j in stopwords.words('english'):
                i.remove(j)

    for i in df_news['Word tokenize']:
        for j in i:
            rootWord= ps.stem(j)
            index = i.index(j)
            i[index] = rootWord
            
    return df_news